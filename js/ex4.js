function doanTamGiac() {
  var canh1Value = document.getElementById("txt-canh1").value * 1;
  var canh2Value = document.getElementById("txt-canh2").value * 1;
  var canh3Value = document.getElementById("txt-canh3").value * 1;
  var tamGiac;
  var ketQuaEl = document.getElementById("txt-ket-qua");

  if (canh1Value == canh2Value && canh2Value == canh3Value) {
    tamGiac = "ĐỀU";
  } else if (
    canh1Value == canh2Value ||
    canh1Value == canh3Value ||
    canh2Value == canh3Value
  ) {
    tamGiac = "CÂN";
  } else if (
    canh1Value * canh1Value ==
      canh2Value * canh2Value + canh3Value * canh3Value ||
    canh2Value * canh2Value ==
      canh1Value * canh1Value + canh3Value * canh3Value ||
    canh3Value * canh3Value == canh1Value * canh1Value + canh2Value * canh2Value
  ) {
    tamGiac = "VUÔNG";
  } else {
    tamGiac = "KHÁC";
  }
  ketQuaEl.innerText = `Là tam giác ${tamGiac}`;
}
