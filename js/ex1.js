function sapXepTangDan() {
  var so1Value = document.getElementById("txt-so1").value;
  var so2Value = document.getElementById("txt-so2").value;
  var so3Value = document.getElementById("txt-so3").value;
  var ketQuaEl = document.getElementById("txt-ket-qua");
  var a, b, c;

  if (so1Value <= so2Value && so1Value <= so3Value) {
    a = so1Value;

    if (so2Value <= so3Value) {
      b = so2Value;
      c = so3Value;
    } else {
      b = so3Value;
      c = so2Value;
    }
  } else if (so2Value <= so1Value && so2Value <= so3Value) {
    a = so2Value;

    if (so1Value <= so3Value) {
      b = so1Value;
      c = so3Value;
    } else {
      b = so3Value;
      c = so1Value;
    }
  } else {
    a = so3Value;

    if (so1Value <= so2Value) {
      b = so1Value;
      c = so2Value;
    } else {
      b = so2Value;
      c = so1Value;
    }
  }

  ketQuaEl.innerText = `Thứ tự tăng dần là: ${a} < ${b} < ${c}`;
}
