function demChanLe() {
  var so1Value = document.getElementById("txt-so1").value * 1;
  var so2Value = document.getElementById("txt-so2").value * 1;
  var so3Value = document.getElementById("txt-so3").value * 1;
  var chan = 0;
  var le = 0;
  var ketQuaEl = document.getElementById("txt-ket-qua");

  if (so1Value % 2 == 0) {
    chan++;

    if (so2Value % 2 == 0 && so3Value % 2 == 0) {
      chan = chan + 2;
    } else if (so2Value % 2 != 0 && so3Value % 2 != 0) {
      chan = chan + 0;
    } else {
      chan++;
    }
  } else if (so2Value % 2 == 0) {
    chan++;

    if (so1Value % 2 == 0 && so3Value % 2 == 0) {
      chan = chan + 2;
    } else if (so1Value % 2 != 0 && so3Value % 2 != 0) {
      chan = chan + 0;
    } else {
      chan++;
    }
  } else if (so3Value % 2 == 0) {
    chan++;

    if (so1Value % 2 == 0 && so2Value % 2 == 0) {
      chan = chan + 2;
    } else if (so1Value % 2 != 0 && so2Value % 2 != 0) {
      chan = chan + 0;
    } else {
      chan++;
    }
  }

  le = 3 - chan;

  ketQuaEl.innerText = `Kết quả là: ${chan} số chẵn và ${le} số lẻ`;
}
